/*
 * Copyright (C) 2009 the_FONZ
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.util.Date;

public class Fonzifier_BackupRestore extends SettingsPreferenceFragment {

    private final String sd = android.os.Environment
                                      .getExternalStorageDirectory().getPath();
    private final String backupdir = File.separator + ".fonz" + File.separator + "backup";
    private final String full_backupdir_location = sd + backupdir;
    private final File directory_backup = new File(full_backupdir_location);
    private final String archivedir = File.separator + ".fonz" + File.separator + "archive";
    private final String full_archivedir_location = sd + archivedir;
    private final File directory_archive = new File(full_archivedir_location);

    // SU shell..
    private static void shell(String shellcommand1, String shellcommand2) {
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("su");
            runtime.exec(new String[]{"su", "-c", shellcommand1});
            runtime.exec(new String[]{"su", "-c", shellcommand2});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.fonzifier_backuprestore);
        final String setperms_open = "chmod 777 ";
        final String setperms_closed = "chmod 660 ";
        final String location_source_db = File.separator + "data" + File.separator + "data" + File.separator +
                                                  "com.android.providers.telephony" + File.separator + "databases" + File.separator + "mmssms.db";
        final String shellcommand1 = setperms_open + location_source_db;
        final String shellcommand2 = " ";

        // Create a new thread and run both methods in the new thread on startup..
        Thread startup_newthread = new Thread(new Runnable() {
            public void run() {
                // create directory structure if not found and set permissions
                structure_check(directory_backup, directory_archive);
                shell(shellcommand1, shellcommand2);
            }
        });
        startup_newthread.start();

        Preference mBackup = findPreference("mBackup");
        mBackup.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                                                 public boolean onPreferenceClick(Preference preference) {
                                                     AlertDialog.Builder alert_mBackup = new AlertDialog.Builder(getActivity());
                                                     alert_mBackup.setTitle("Create Backup ?...");
                                                     alert_mBackup.setMessage("Do you wish to continue ?... \n\nThis will start the backup process");
                                                     alert_mBackup.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                         public void onClick(DialogInterface dialog, int whichButton) {
                                                             // set locations and file names to pass to backup/restore function
                                                             final String source = location_source_db;
                                                             final String currentDateandTime = DateFormat.getDateTimeInstance().format(new Date());
                                                             final String backupname = "SMS-Backup_" + currentDateandTime + ".fonz";
                                                             final String output = sd + backupdir + File.separator + backupname;
                                                             final String event = "Backup";

                                                             File directory = new File(full_backupdir_location);
                                                             File[] contents = directory.listFiles();
                                                             // the directory file is not really a directory/not found..
                                                             if (contents == null) {
                                                                 Toast.makeText(getActivity(), "Error, No backup folder found!. \n\nPlease restart the app...", Toast.LENGTH_SHORT).show();
                                                             }
                                                             // Folder is empty..
                                                             else if (contents.length == 0) {
                                                                 Toast.makeText(getActivity(), "Creating new Backup...", Toast.LENGTH_SHORT).show();
                                                                 backup_restore(source, output, event, location_source_db, setperms_open, setperms_closed);
                                                             }
                                                             // Folder contains files..
                                                             else {
                                                                 Toast.makeText(getActivity(), "Previous Backup Found! \n\nArchiving Previous Backup...", Toast.LENGTH_SHORT).show();
                                                                 File sourceLocation = new File(full_backupdir_location);
                                                                 File targetLocation = new File(full_archivedir_location);
                                                                 try {
                                                                     copyDirectory(sourceLocation, targetLocation);
                                                                 } catch (IOException e) {
                                                                     e.printStackTrace();
                                                                 }
                                                                 File target = new File(full_backupdir_location);
                                                                 DeleteRecursive(target);
                                                                 structure_check(directory_backup, directory_archive);
                                                                 Toast.makeText(getActivity(), "Creating new Backup...", Toast.LENGTH_SHORT).show();
                                                                 backup_restore(source, output, event, location_source_db, setperms_open, setperms_closed);
                                                             }

                                                         }
                                                     });
                                                     alert_mBackup.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                                         public void onClick(DialogInterface dialog, int whichButton) {
                                                             // Canceled.
                                                         }
                                                     });
                                                     alert_mBackup.show();
                                                     return true;
                                                 }
                                             }
        );

        Preference mRestore = findPreference("mRestore");
        mRestore.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                                                  public boolean onPreferenceClick(Preference preference) {
                                                      // AlertDialog.Builder alert_mRestore = new AlertDialog.Builder(getActivity());
                                                      // alert_mRestore.setTitle("Restore Current Backup ?...");
                                                      // alert_mRestore.setMessage("Do you wish to continue ?... \n\nThis will start the restore process");
                                                      // alert_mRestore.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                      //     public void onClick(DialogInterface dialog, int whichButton) {
                                                      //         for (File f : directory_archive.listFiles()) {
                                                      //              if (f.isFile()) {
                                                      //                final String backup_name = f.getName();
                                                      //                // Log.i("file names", backup_name);
                                                      //              }
                                                      //          }


                                                      //         // set locations and file names to pass to backup/restore function
                                                      //         final String source = full_archivedir_location + backup_name;
                                                      //         final String output = location_source_db;
                                                      //         final String event = "Restore";

                                                      //         File directory = new File(full_backupdir_location);
                                                      //         File[] contents = directory.listFiles();
                                                      //         // the directory file is not really a directory/not found..
                                                      //         if (contents == null) {
                                                      //             Toast.makeText(getActivity(), "Error, No backup folder found!. \n\nPlease restart the app...", Toast.LENGTH_SHORT).show();
                                                      //         }
                                                      //         // Folder is empty..
                                                      //         else if (contents.length == 0) {
                                                      //             Toast.makeText(getActivity(), "No Current Backup Found To Restore!", Toast.LENGTH_SHORT).show();
                                                      //         }
                                                      //         // Folder contains files..
                                                      //         else {
                                                      //             Toast.makeText(getActivity(), "Current Backup Found! \n\nRestoring Current Backup...", Toast.LENGTH_SHORT).show();
                                                      //             backup_restore(source, output, event, location_source_db, setperms_open, setperms_closed);
                                                      //         }

                                                      //     }
                                                      // });
                                                      // alert_mRestore.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                                      //     public void onClick(DialogInterface dialog, int whichButton) {
                                                      //         // Canceled.
                                                      //     }
                                                      // });
                                                      // alert_mRestore.show();
                                                      Toast.makeText(getActivity(), "mRestore dummy string", Toast.LENGTH_SHORT).show();
                                                      return true;
                                                  }
                                              }
        );

        Preference mRestoreArc = findPreference("mRestoreArc");
        mRestoreArc.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                                                     public boolean onPreferenceClick(Preference preference) {
                                                         Toast.makeText(getActivity(), "mRestoreArc dummy string", Toast.LENGTH_SHORT).show();
                                                         return true;
                                                     }
                                                 }
        );

        Preference mArchive = findPreference("mDeleteCur");
        mArchive.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                                                  public boolean onPreferenceClick(Preference preference) {
                                                      if (directory_backup.exists()) {
                                                          AlertDialog.Builder alert_mDeleteCur = new AlertDialog.Builder(getActivity());
                                                          alert_mDeleteCur.setTitle("Delete Current Backup ?...");
                                                          alert_mDeleteCur.setMessage("Do you wish to continue ?... \n\nThis will remove the current backup");
                                                          alert_mDeleteCur.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                              public void onClick(DialogInterface dialog, int whichButton) {
                                                                  Toast.makeText(getActivity(), "Removing Current Backup...", Toast.LENGTH_SHORT).show();
                                                                  File target = new File(full_backupdir_location);
                                                                  DeleteRecursive(target);
                                                                  structure_check(directory_backup, directory_archive);
                                                                  Toast.makeText(getActivity(), "Current Backup Removed", Toast.LENGTH_SHORT).show();
                                                              }
                                                          });
                                                          alert_mDeleteCur.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                                              public void onClick(DialogInterface dialog, int whichButton) {
                                                                  // Canceled.
                                                              }
                                                          });
                                                          alert_mDeleteCur.show();
                                                      } else {
                                                          Toast.makeText(getActivity(), "No Current Backup Found...", Toast.LENGTH_SHORT).show();
                                                      }
                                                      return true;
                                                  }
                                              }
        );

        Preference mDeleteArc = findPreference("mDeleteArc");
        mDeleteArc.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                                                    public boolean onPreferenceClick(Preference preference) {
                                                        if (directory_archive.exists()) {
                                                            AlertDialog.Builder alert_mDeleteArc = new AlertDialog.Builder(getActivity());
                                                            alert_mDeleteArc.setTitle("Delete Archived Backups ?...");
                                                            alert_mDeleteArc.setMessage("Do you wish to continue ?... \n\nThis will remove ALL archived backups");
                                                            alert_mDeleteArc.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                                    Toast.makeText(getActivity(), "Removing Archived Backups...", Toast.LENGTH_SHORT).show();
                                                                    File target = new File(full_archivedir_location);
                                                                    DeleteRecursive(target);
                                                                    structure_check(directory_backup, directory_archive);
                                                                    Toast.makeText(getActivity(), "Archived Backups Removed", Toast.LENGTH_SHORT).show();
                                                                }
                                                            });
                                                            alert_mDeleteArc.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                                    // Canceled.
                                                                }
                                                            });
                                                            alert_mDeleteArc.show();
                                                        } else {
                                                            Toast.makeText(getActivity(), "No Current Backup Found...", Toast.LENGTH_SHORT).show();
                                                        }
                                                        return true;
                                                    }
                                                }
        );
    }

    // Startup folder structure check..
    private void structure_check(File directory_backup, File directory_archive) {
        if (!directory_backup.exists()) {
            //noinspection ResultOfMethodCallIgnored
            directory_backup.mkdirs();
        }
        if (!directory_archive.exists()) {
            //noinspection ResultOfMethodCallIgnored
            directory_archive.mkdirs();
        }
    }

    // Backup & Restore..
    private void backup_restore(String source, String output, String event, String location_source_db, String setperms_open, String setperms_closed) {
        InputStream myInput;

        try {
            //noinspection UnnecessaryLocalVariable
            String shellcommand1 = setperms_open + location_source_db;
            String shellcommand2 = " ";
            shell(shellcommand1, shellcommand2);

            myInput = new FileInputStream(source);
            final OutputStream myOutput = new FileOutputStream(output);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            myOutput.flush();
            myOutput.close();
            Toast.makeText(getActivity(), event + " Complete", Toast.LENGTH_SHORT).show();
            myInput.close();

        } catch (FileNotFoundException e) {
            Toast.makeText(getActivity(), event + " error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (IOException e) {
            Toast.makeText(getActivity(), event + " error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        String shellcommand1 = setperms_closed + location_source_db;
        String shellcommand2 = " ";
        shell(shellcommand1, shellcommand2);
    }

    // Copy dir contents to another dir (ie backup > archive) If targetLocation does not exist, it will be created.
    void copyDirectory(File sourceLocation, File targetLocation)
            throws IOException {

        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists() && !targetLocation.mkdirs()) {
                throw new IOException("Cannot create dir " + targetLocation.getAbsolutePath());
            }

            String[] children = sourceLocation.list();
            for (String aChildren : children) {
                copyDirectory(new File(sourceLocation, aChildren),
                                     new File(targetLocation, aChildren));
            }
        } else {
            // make sure the target directory exists
            File directory = targetLocation.getParentFile();
            if (directory != null && !directory.exists() && !directory.mkdirs()) {
                throw new IOException("Cannot create dir " + directory.getAbsolutePath());
            }

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }

    // Delete recursive..
    private void DeleteRecursive(File target) {
        if (target.isDirectory())
            for (File child : target.listFiles())
                DeleteRecursive(child);
        target.delete();
    }
}
